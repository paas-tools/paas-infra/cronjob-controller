# CronJob controller
This Kubernetes controller takes care of setting a default value for the following [CronJob](https://kubernetes.io/docs/concepts/workloads/controllers/cron-jobs/) variables:

* `.Spec.JobTemplate.Spec.ActiveDeadlineSeconds`
* `.Spec.SuccessfulJobsHistoryLimit`
* `.Spec.FailedJobsHistoryLimit`

The default value of these fields can be overridden through command line flags.

The controller will act on CronJobs from all namespaces unless the `--namespace` flag is passed.

The controller is heavily based on this [Kubernetes Controller example](https://github.com/kubernetes/client-go/tree/master/examples/workqueue) and the
[official controller recommendations](https://git.k8s.io/community/contributors/devel/controllers.md). It
is designed to be run in-cluster and to authenticate to the Kubernetes API from the
default service account location (ie `/var/run/kubernetes/serviceaccount/token`)

### Deployment
The e-group sync is deployed in both `openshift.cern.ch` and `openshift-dev.cern.ch` in the `paas-infra` namespace.
For instructions on how to deploy the namespace, check the [OneNote docs](https://espace.cern.ch/openshift-internal/_layouts/15/WopiFrame.aspx?sourcedoc=%2Fopenshift%2Dinternal%2FShared%20Documents%2FOpenshift&action=edit&wd=target%28%2F%2FDeployment.one%7Cdacb26c0-65a0-4eee-8b63-99cfeaef4a66%2F%27paas-infra%27%20namespace%7C9a78943f-4902-48c6-a972-2a5b18e8e901%2F%29)

The account running the cronjob-controller needs permissions to edit CronJobs in all namespaces,
so we are going to assign it the `cluster-admin` role.
```
oc create serviceaccount cronjob-controller -n paas-infra
oc adm policy add-cluster-role-to-user cluster-admin -z cronjob-controller -n paas-infra
```

Once this is done, just create the application by running:
```
oc create -f deploy/ -n paas-infra
```

### Continuous Integration

The deployment of the application is managed by GitLab-CI. Any changes to the definition and config
files will be automatically redeployed into dev and production when merged to `master`.

For the development purposes, two manual triggers are included in the CI definition to allow easy deployment of
custom branches into the application running in the dev [environment](https://openshift-dev.cern.ch).

Check the [OneNote docs](https://espace.cern.ch/openshift-internal/_layouts/15/WopiFrame.aspx?sourcedoc=%2Fopenshift%2Dinternal%2FShared%20Documents%2FOpenshift&action=edit&wd=target%28%2F%2FDeployment.one%7Cdacb26c0-65a0-4eee-8b63-99cfeaef4a66%2F%27paas-infra%27%20namespace%7C9a78943f-4902-48c6-a972-2a5b18e8e901%2F%29) for instructions on how to set up the Continuos Integration and Continuous Deployment for this project.
