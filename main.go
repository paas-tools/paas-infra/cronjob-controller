/*
Copyright 2017 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"fmt"
	"time"

	flag "github.com/spf13/pflag"

	"github.com/golang/glog"

	"k8s.io/api/batch/v2alpha1"
	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apimachinery/pkg/util/runtime"
	"k8s.io/apimachinery/pkg/util/wait"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/cache"
	"k8s.io/client-go/util/workqueue"
)

// Default value to apply to all cronjobs without a custom value for ActiveDeadlineSeconds,
// successfulJobsHistoryLimit and failedJobsHistoryLimit
var defaultActiveDeadlineSeconds int64
var defaultSuccessfulJobsHistoryLimit int32
var defaultFailedJobsHistoryLimit int32

// Dry run mode
var dryRun bool

type Controller struct {
	indexer  cache.Indexer
	queue    workqueue.RateLimitingInterface
	informer cache.Controller
	client   kubernetes.Interface
}

func NewController(queue workqueue.RateLimitingInterface, indexer cache.Indexer, informer cache.Controller, client kubernetes.Interface) *Controller {
	return &Controller{
		informer: informer,
		indexer:  indexer,
		queue:    queue,
		client:   client,
	}
}

func (c *Controller) processNextItem() bool {
	// Wait until there is a new item in the working queue
	key, quit := c.queue.Get()
	if quit {
		return false
	}
	// Tell the queue that we are done with processing this key. This unblocks the key for other workers
	// This allows safe parallel processing because two objects with the same key are never processed in
	// parallel.
	defer c.queue.Done(key)

	// Invoke the method containing the business logic
	err := c.doWork(key.(string))
	// Handle the error if something went wrong during the execution of the business logic
	c.handleErr(err, key)
	return true
}

// doWork is the business logic of the controller. In case an error happened, it has to simply return the error.
// The retry logic should not be part of the business logic.
func (c *Controller) doWork(key string) error {
	obj, exists, err := c.indexer.GetByKey(key)
	if err != nil {
		glog.Errorf("Fetching object with key %s from store failed with %v", key, err)
		return err
	}

	// If the object has been deleted, do nothing in this run
	if !exists {
		return nil
	}
	// Note that you also have to check the uid if you have a local controlled resource, which
	// is dependent on the actual instance, to detect that an object was recreated with the same name
	cronjob := obj.(*v2alpha1.CronJob)
	// Obtain a client to be able to modify a cronjob in this namespace
	cronjobClient := c.client.BatchV2alpha1().CronJobs(cronjob.GetNamespace())
	// Use a bool flag to decide if the cronjob needs to be changed or not
	needToUpdate := false

	// Check and set activeDeadlineSeconds
	if activeDeadlineSeconds := cronjob.Spec.JobTemplate.Spec.ActiveDeadlineSeconds; activeDeadlineSeconds == nil {
		fmt.Println("'activeDeadlineSeconds' is not set for this cronjob")
		// Setting activeDeadlineSeconds to the default value
		if !dryRun {
			cronjob.Spec.JobTemplate.Spec.ActiveDeadlineSeconds = &defaultActiveDeadlineSeconds
			needToUpdate = true
		} else {
			fmt.Println("dry-run mode: Not changing the object...")
		}
		fmt.Printf("Setting ActiveDeadlineSeconds to default value of '%d'\n", defaultActiveDeadlineSeconds)
	}
	// Check and set successfulJobsHistoryLimit
	if successfulJobsHistoryLimit := cronjob.Spec.SuccessfulJobsHistoryLimit; successfulJobsHistoryLimit == nil {
		fmt.Println("'successfulJobsHistoryLimit' is not set for this cronjob")
		// Setting successfulJobsHistoryLimit to the default value
		if !dryRun {
			cronjob.Spec.SuccessfulJobsHistoryLimit = &defaultSuccessfulJobsHistoryLimit
			needToUpdate = true
		} else {
			fmt.Println("dry-run mode: Not changing the object...")
		}
		fmt.Printf("Setting successfulJobsHistoryLimit to default value of '%d'\n", defaultSuccessfulJobsHistoryLimit)
	}

	// Check and set failedJobsHistoryLimit
	if failedJobsHistoryLimit := cronjob.Spec.FailedJobsHistoryLimit; failedJobsHistoryLimit == nil {
		fmt.Println("'failedJobsHistoryLimit' is not set for this cronjob")
		// Setting successfulJobsHistoryLimit to the default value
		if !dryRun {
			cronjob.Spec.FailedJobsHistoryLimit = &defaultFailedJobsHistoryLimit
			needToUpdate = true
		} else {
			fmt.Println("dry-run mode: Not changing the object...")
		}
		fmt.Printf("Setting failedJobsHistoryLimit to default value of '%d'\n", defaultFailedJobsHistoryLimit)
	}

	if needToUpdate {
		fmt.Printf("At least one of the previous values was changed, updating cronjob %s/%s...\n",
			cronjob.GetNamespace(), cronjob.GetName())
		savedCronJob, cronjobErr := cronjobClient.Update(cronjob)
		if cronjobErr != nil {
			return cronjobErr
		}
		fmt.Printf("CronJob '%s' successfully updated!\n", savedCronJob.GetName())
	}
	return nil
}

// handleErr checks if an error happened and makes sure we will retry later.
func (c *Controller) handleErr(err error, key interface{}) {
	if err == nil {
		// Forget about the #AddRateLimited history of the key on every successful synchronization.
		// This ensures that future processing of updates for this key is not delayed because of
		// an outdated error history.
		c.queue.Forget(key)
		return
	}

	// This controller retries 5 times if something goes wrong. After that, it stops trying.
	if c.queue.NumRequeues(key) < 5 {
		glog.Infof("Error syncing object %v: %v", key, err)

		// Re-enqueue the key rate limited. Based on the rate limiter on the
		// queue and the re-enqueue history, the key will be processed later again.
		c.queue.AddRateLimited(key)
		return
	}

	c.queue.Forget(key)
	// Report to an external entity that, even after several retries, we could not successfully process this key
	runtime.HandleError(err)
	glog.Infof("Dropping Object %q out of the queue: %v", key, err)
}

func (c *Controller) Run(threadiness int, stopCh chan struct{}) {
	defer runtime.HandleCrash()

	// Let the workers stop when we are done
	defer c.queue.ShutDown()
	glog.Info("Starting Kubernetes controller")

	go c.informer.Run(stopCh)

	// Wait for all involved caches to be synced, before processing items from the queue is started
	if !cache.WaitForCacheSync(stopCh, c.informer.HasSynced) {
		runtime.HandleError(fmt.Errorf("Timed out waiting for caches to sync"))
		return
	}

	for i := 0; i < threadiness; i++ {
		go wait.Until(c.runWorker, time.Second, stopCh)
	}

	<-stopCh
	glog.Info("Stopping Kubernetes controller")
}

func (c *Controller) runWorker() {
	for c.processNextItem() {
	}
}

func main() {
	var namespace string

	flag.StringVar(&namespace, "namespace", "", "Namespace to watch. Do not set to watch all namespaces")
	flag.Int64Var(&defaultActiveDeadlineSeconds, "defaultActiveDeadlineSeconds", 300, "Number of ActiveDeadlineSeconds to set on CronJobs")
	flag.Int32Var(&defaultSuccessfulJobsHistoryLimit, "defaultSuccessfulJobsHistoryLimit", 3, "Number of SuccessfulJobsHistoryLimit to set on Cronjobs")
	flag.Int32Var(&defaultFailedJobsHistoryLimit, "defaultFailedJobsHistoryLimit", 1, "Number of failedJobsHistoryLimit to set on CronJobs")
	flag.BoolVar(&dryRun, "dry-run", false, "Set flag to only print the operations instead of actually doing them")

	flag.Parse()

	// creates the connection
	config, err := rest.InClusterConfig()
	if err != nil {
		glog.Fatal(err)
	}

	// creates the clientset
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		glog.Fatal(err)
	}

	// create the CronJob watcher
	cronJobListWatcher := cache.NewListWatchFromClient(clientset.BatchV2alpha1().RESTClient(), "cronjobs", namespace, fields.Everything())
	if namespace != "" {
		fmt.Printf("Watching CronJobs for namespace '%s'\n", namespace)
	} else {
		fmt.Println("Watching CronJobs for all namespaces")
	}

	if dryRun {
		fmt.Println("Running in dry-run mode, operations will not change data.")
	}

	// create the workqueue
	queue := workqueue.NewRateLimitingQueue(workqueue.DefaultControllerRateLimiter())

	// Bind the workqueue to a cache with the help of an informer. This way we make sure that
	// whenever the cache is updated, the Object key is added to the workqueue.
	// Note that when we finally process the item from the workqueue, we might see a newer version
	// of the Object than the version which was responsible for triggering the update.
	indexer, informer := cache.NewIndexerInformer(cronJobListWatcher, &v2alpha1.CronJob{}, 0, cache.ResourceEventHandlerFuncs{
		AddFunc: func(obj interface{}) {
			key, err := cache.MetaNamespaceKeyFunc(obj)
			if err == nil {
				queue.Add(key)
			}
		},
		UpdateFunc: func(old interface{}, new interface{}) {
			key, err := cache.MetaNamespaceKeyFunc(new)
			if err == nil {
				queue.Add(key)
			}
		},
		DeleteFunc: func(obj interface{}) {
			// IndexerInformer uses a delta queue, therefore for deletes we have to use this
			// key function.
			key, err := cache.DeletionHandlingMetaNamespaceKeyFunc(obj)
			if err == nil {
				queue.Add(key)
			}
		},
	}, cache.Indexers{})

	controller := NewController(queue, indexer, informer, clientset)

	// Now let's start the controller
	stop := make(chan struct{})
	defer close(stop)
	go controller.Run(1, stop)

	// Wait forever
	select {}
}
